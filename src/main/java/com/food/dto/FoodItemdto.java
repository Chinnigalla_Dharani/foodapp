package com.food.dto;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor

public class FoodItemdto {
	 @NotBlank(message = "Item name cannot be blank")
	private String itemName;
	 @NotBlank(message = "Item type cannot be blank")
	private String itemType;
	 //@Positive(message = "Item price must be positive")
	 //@NotBlank(message = "Item price cannot be null")
	 @Min(0)
	 @Max(10000)
	private int itemPrice;
}
