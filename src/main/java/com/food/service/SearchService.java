package com.food.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.food.entity.FoodItem;
import com.food.repository.SearchRepository;

@Service
public class SearchService {
	@Autowired
	private SearchRepository searchRepository;
	public List<FoodItem> searchFoodItems1(String itemName, String itemType) {
        if (itemName != null && itemType != null) {
            return searchRepository.findByItemNameContainingIgnoreCaseAndItemTypeContainingIgnoreCase(itemName, itemType);
        }
         else if (itemName != null) {
           return searchRepository.findByItemNameContainingIgnoreCase(itemName);
        } else if (itemType != null) {
            return searchRepository.findByItemTypeContainingIgnoreCase(itemType);
        } else {
       
            return searchRepository.findAll();
        }
    }
}
