package com.food.service;

import java.util.Optional;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.food.dto.LoginStatus;
import com.food.dto.ResponseDTO;
import com.food.dto.UserDTO;
import com.food.entity.UserRegistration;
import com.food.exceptions.UserAlreadyExists;
import com.food.repository.UserRepository;

import jakarta.validation.Valid;

@Service
public class UserService {
	@Autowired
	private UserRepository userRepository;
 
	public ResponseDTO register(UserDTO userDto) {
		Optional<UserRegistration> user = userRepository.findByuserName(userDto.getUserName());
		if (user.isPresent()) {
			throw new UserAlreadyExists("User already Registered");
		}
		UserRegistration users = new UserRegistration();
		users.setUserName(userDto.getUserName());
		users.setEmailId(userDto.getEmailId());
		users.setPhoneNo(userDto.getPhoneNo());
		//users.setLog(LoginStatus.LoggedOut);
		String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~`!@#$%^&*()";
		String pwd = RandomStringUtils.random(8, characters);
		users.setPassword(pwd);
		users.setUserRole(userDto.getUserRole());
 
		userRepository.save(users);
		return ResponseDTO.builder().StatusCode(201).message("User Registered sucessfully").build();
	
	
	}}
