package com.food.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.food.dto.FoodItemdto;
import com.food.dto.OrderDTO;
import com.food.dto.OrderResponseDto;
import com.food.entity.FoodItem;
import com.food.entity.OrderEntity;
import com.food.entity.UserRegistration;
import com.food.exceptions.ItemAlreadyExists;
import com.food.repository.Fooditemrepository;
import com.food.repository.OrderRepository;
import com.food.repository.UserRepository;

import jakarta.validation.Valid;

@Service
public class OrderService {
	@Autowired
	private OrderRepository orderRepository;
	@Autowired
	private Fooditemrepository fooditemrepository;
	@Autowired
	private UserRepository userRepository;

	public OrderResponseDto addOrder(int userId, int itemId, OrderDTO orderDTO) {
		Optional<UserRegistration> registrationOptional = userRepository.findByUserId(userId);
		
		UserRegistration user  = new UserRegistration();
		user.setUserId(userId);
		
		Optional<FoodItem> registrationfood = fooditemrepository.findByItemId(itemId);
		
		FoodItem foodItem  = new FoodItem();
		foodItem.setItemId(itemId);
		OrderEntity order = new OrderEntity();
		order.setQuantity(orderDTO.getQuantity());
		order.setTotalAmount(orderDTO.getTotalamount());
		order.setUserRegistration(user);
		order.setFoodItem(foodItem);
		orderRepository.save(order);
		OrderResponseDto orderResponseDto = new OrderResponseDto();
		orderResponseDto.setStatusCode(201l);
		orderResponseDto.setMessage("Ordered successfully..");
		return orderResponseDto;
		
	}
}

