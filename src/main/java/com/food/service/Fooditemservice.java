package com.food.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.food.dto.FoodItemdto;
import com.food.entity.FoodItem;
import com.food.exceptions.ItemAlreadyExists;
import com.food.repository.Fooditemrepository;


@Service
public class Fooditemservice {
	@Autowired
	private Fooditemrepository fooditemrepository;

	public void addItem(FoodItemdto food) {
		Optional<FoodItem> foodit = fooditemrepository.findByItemName(food.getItemName());
		if (foodit.isPresent()) {
			throw new ItemAlreadyExists("Item already added");
		}
		
		FoodItem fooditem = new FoodItem();
		
		fooditem.setItemName(food.getItemName());
		fooditem.setItemType(food.getItemType());
		fooditem.setItemPrice(food.getItemPrice());
		fooditemrepository.save(fooditem);
	}

	public FoodItem getItemById(int itemId) {
		// TODO Auto-generated method stub
		return null;
	}

}
