package com.food.entity;

import java.time.LocalDateTime;

import com.food.dto.LoginStatus;
import com.food.dto.UserRole;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
@Entity
 
public class UserRegistration {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int userId;
 
	private String userName;
	private String emailId;
	private String phoneNo;
	private String password;
 
	@Enumerated(EnumType.STRING)
	private UserRole userRole;
 
	@Enumerated(EnumType.STRING)
	private LoginStatus log;

//	//public void setItemId(int itemId) {
//		// TODO Auto-generated method stub
//		
//	}//
// 
}
