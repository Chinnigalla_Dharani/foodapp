package com.food.entity;

import java.time.LocalDateTime;

import org.hibernate.annotations.CreationTimestamp;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
@Entity
 
public class OrderEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int orderId;
	private int quantity;
	private double totalAmount;
 
	@Column(updatable = false)
	@CreationTimestamp
	private LocalDateTime orderedTime;
	
	@ManyToOne
	@JoinColumn(name = "itemId")
	private FoodItem foodItem;
	
	@ManyToOne
	@JoinColumn(name = "user_Id")
	private UserRegistration userRegistration;
 
}