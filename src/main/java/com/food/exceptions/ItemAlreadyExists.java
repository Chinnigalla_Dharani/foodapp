package com.food.exceptions;

public class ItemAlreadyExists extends RuntimeException{
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public ItemAlreadyExists(String message) {
       super(message);
   }
}

