package com.food.exceptions;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.food.dto.ResponseDTO;



@RestControllerAdvice
public class GlobalExceptionHandler {
	@ResponseStatus(HttpStatus.BAD_REQUEST)
		@ExceptionHandler(MethodArgumentNotValidException.class)
		public Map<String, String> handleinvaliddata(MethodArgumentNotValidException ex) {
			Map<String, String> errMap = new HashMap<>();
			ex.getBindingResult().getFieldErrors().forEach(err -> {
				errMap.put(err.getField(), err.getDefaultMessage());
			});
			return errMap;
	}
	@ExceptionHandler(ItemAlreadyExists.class)
	public ResponseEntity<ResponseDTO> handleItemAlreadyExists(ItemAlreadyExists exception) {
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ResponseDTO(exception.getMessage(), 404));
 
	}
	@ExceptionHandler(UserAlreadyExists.class)
	public ResponseEntity<ResponseDTO> handleUserAlreadyExists(UserAlreadyExists exception) {
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ResponseDTO(exception.getMessage(), 404));
 
	}
}
