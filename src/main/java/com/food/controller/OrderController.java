package com.food.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.food.dto.OrderDTO;
import com.food.dto.OrderResponseDto;
import com.food.service.OrderService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/order")
public class OrderController {
	@Autowired
	private OrderService  orderService;

	@PostMapping("/{userId}/{itemId}")
	public ResponseEntity<OrderResponseDto> addOrder(@PathVariable int userId,@PathVariable int itemId, @RequestBody @Valid OrderDTO orderDTO) {
		OrderResponseDto responsedto=orderService.addOrder(userId,itemId,orderDTO);
		return new ResponseEntity<>(responsedto,HttpStatus.CREATED);
	}
}
