package com.food.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.food.dto.FoodItemdto;
import com.food.service.Fooditemservice;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/fooditems")
public class FoodItemcontroller {
	@Autowired
	private Fooditemservice  fooditemservice ;

	@PostMapping("/")
	public ResponseEntity<String> addItem( @RequestBody @Valid FoodItemdto foodItemdto) {
		fooditemservice.addItem(foodItemdto);
		return new ResponseEntity<>("Item added successfully", HttpStatus.CREATED);
	}
}
