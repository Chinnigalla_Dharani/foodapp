package com.food.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.food.entity.FoodItem;
import com.food.service.SearchService;

@RequestMapping("/foods")
@RestController
public class SearchController {
	@Autowired 
	private SearchService searchService;
	@GetMapping
    public ResponseEntity<List<FoodItem>> searchFoodItems(
            @RequestParam(required = false) String itemName,
            @RequestParam(required = false) String itemType) {
        List<FoodItem> foodItems = searchService.searchFoodItems1(itemName, itemType);
        return new ResponseEntity<>(foodItems, HttpStatus.OK);
    }
}

