package com.food.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.food.dto.ResponseDTO;
import com.food.dto.UserDTO;
import com.food.service.UserService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("api")
public class UserController {
	@Autowired
	private UserService userService;
 
	@PostMapping("/user")
	public ResponseEntity<ResponseDTO> register(@Valid @RequestBody UserDTO userDTO) {
		return new ResponseEntity<ResponseDTO>(userService.register(userDTO), HttpStatus.CREATED);
	}
}