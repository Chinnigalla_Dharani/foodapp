package com.food.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.food.dto.OrderDTO;
import com.food.entity.FoodItem;
import com.food.entity.OrderEntity;

public interface OrderRepository extends JpaRepository<OrderEntity, Integer>  {

	void save(OrderDTO order);

	//Optional<FoodItem> findByItemName(String itemName);
}
