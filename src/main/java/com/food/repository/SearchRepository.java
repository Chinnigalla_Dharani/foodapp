package com.food.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.food.entity.FoodItem;

public interface SearchRepository extends JpaRepository<FoodItem, Integer>{

	List<FoodItem> findByItemNameContainingIgnoreCaseAndItemTypeContainingIgnoreCase(String itemName, String itemType);

	List<FoodItem> findByItemNameContainingIgnoreCase(String itemName);

	List<FoodItem> findByItemTypeContainingIgnoreCase(String itemType);

}
