package com.food.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.food.entity.FoodItem;
import com.food.entity.UserRegistration;

public interface Fooditemrepository extends JpaRepository<FoodItem, Integer> {

	Optional<FoodItem> findByItemName(String itemName);

	//Optional<UserRegistration> findByUserId(int user_Id);

	Optional<FoodItem> findByItemId(int itemId);

	//Optional<FoodItem> findByItemName(Object itemName);

}
