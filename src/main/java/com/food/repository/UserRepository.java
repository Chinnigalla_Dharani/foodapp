package com.food.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.food.entity.UserRegistration;

public interface UserRepository extends JpaRepository<UserRegistration, Integer> {

	Optional<UserRegistration> findByuserName(String userName);

	//Optional<UserRegistration> findByItemId(int itemId);

	Optional<UserRegistration> findByUserId(int userId);
}
